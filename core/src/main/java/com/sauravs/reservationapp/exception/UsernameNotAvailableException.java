package com.sauravs.reservationapp.exception;

public class UsernameNotAvailableException extends NotAvailableException {

    private static final long serialVersionUID = -5179305029574748974L;

    public UsernameNotAvailableException() {
        super("Username is not available.");
    }
}

