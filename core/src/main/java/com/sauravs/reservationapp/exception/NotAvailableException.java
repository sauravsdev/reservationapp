package com.sauravs.reservationapp.exception;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
class NotAvailableException extends RuntimeException {

    private static final long serialVersionUID = -5588191333233268847L;

    NotAvailableException(String message) {
        super(message);
    }
}

