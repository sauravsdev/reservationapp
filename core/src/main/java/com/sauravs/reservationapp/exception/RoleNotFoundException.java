package com.sauravs.reservationapp.exception;

public class RoleNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 2897249599003620637L;

    public RoleNotFoundException() {
        super("Role could not be found.");
    }
}
