package com.sauravs.reservationapp.exception;

public class CodeNotAvailableException extends NotAvailableException {

    private static final long serialVersionUID = -3328098796810410637L;

    public CodeNotAvailableException() {
        super("Code is not available.");
    }
}
