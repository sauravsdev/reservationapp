package com.sauravs.reservationapp.exception;

public class UserNotFoundException extends NotFoundException {

    private static final long serialVersionUID = -4070709476280623549L;

    public UserNotFoundException() {
        super("User could not be found.");
    }
}

