package com.sauravs.reservationapp.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3049274646559292304L;

    public NotFoundException() {
    }

    public NotFoundException(String message) {
        super(message);
    }
}
