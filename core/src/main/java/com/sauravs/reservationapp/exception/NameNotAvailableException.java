package com.sauravs.reservationapp.exception;

public class NameNotAvailableException extends NotAvailableException {

    private static final long serialVersionUID = -3332481913134559058L;

    public NameNotAvailableException() {
        super("Name is not available.");
    }
}
