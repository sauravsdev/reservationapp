package com.sauravs.reservationapp.service;

import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.query.RoleQuery;
import com.sauravs.reservationapp.query.UserQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface UserService {

    @Nonnull
    Role saveRole(@Nonnull final Role role);

    void deleteRole(@Nonnull final Long id);

    Optional<Role> findRoleById(@Nonnull final Long id);

    @Nonnull
    Role roleById(@Nonnull final Long id);

    Optional<Role> findRoleByName(@Nonnull final String name);

    @Nonnull
    Role roleByName(@Nonnull final String name);

    Optional<Role> findRoleByCode(@Nonnull final String code);

    @Nonnull
    Role roleByCode(@Nonnull final String code);

    @Nonnull
    Page<Role> findAllRole(@Nonnull final Pageable pageable);

    @Nonnull
    Page<Role> findAllRoleWithQuery(@Nonnull final Pageable pageable, @Nonnull final RoleQuery query);


    @Nonnull
    User saveUser(@Nonnull final User user);

    void deleteUser(@Nonnull final Long id);

    Optional<User> findUserById(@Nonnull final Long id);

    @Nonnull
    User userById(@Nonnull final Long id);

    Optional<User> findUserByUsername(@Nonnull final String username);

    @Nonnull
    User userByUsername(@Nonnull final String username);

    Optional<User> findUserByCode(@Nonnull final String code);

    @Nonnull
    User userByCode(@Nonnull final String code);

    Optional<User> findUserWithRolesByUsername(@Nonnull final String username);

    @Nonnull
    User userWithRolesByUsername(@Nonnull final String username);

    @Nonnull
    Page<User> findAllUser(@Nonnull final Pageable pageable);

    @Nonnull
    Page<User> findAllUserWithQuery(@Nonnull final Pageable pageable, @Nonnull final UserQuery query);

}