package com.sauravs.reservationapp.converter;

import com.sauravs.reservationapp.model.UserStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;


@Converter(autoApply = true)
public class UserStatusConverter implements AttributeConverter<UserStatus, Integer> {
    @Override
    public Integer convertToDatabaseColumn(UserStatus attribute) {
        return attribute == null ? null : attribute.getId();
    }

    @Override
    public UserStatus convertToEntityAttribute(Integer dbData) {
        return dbData == null ? null : UserStatus.getById(dbData);
    }
}