package com.sauravs.reservationapp.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@MappedSuperclass
public abstract class BaseEntity implements Persistable, Identifiable<Long> {

    private static final long serialVersionUID = -1659513470959395583L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public boolean isPersisted() {
        return id != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if ((o == null) || (getClass() != o.getClass()))
            return false;

        BaseEntity that = (BaseEntity) o;

        return (id != null) && (id.equals(that.id));
    }

    @Override
    public int hashCode() {
        int hashCode = 13;
        hashCode += null == id ? 0 : id.hashCode() * 19;
        return hashCode;
    }

    @Override
    public String toString() {
        return "Entity " + getClass().getSimpleName() + '[' + "id=" + id + ']';
    }

}
