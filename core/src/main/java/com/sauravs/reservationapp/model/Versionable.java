package com.sauravs.reservationapp.model;

public interface Versionable {
    Long getVersion();
}
