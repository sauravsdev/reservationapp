package com.sauravs.reservationapp.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(
    name = "users",
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = "username",
            name = "uk_users_username"
        ),
        @UniqueConstraint(
            columnNames = "code",
            name = "uk_users_code"
        )
    }
)
public class User extends BaseEntity implements Versionable {

    private static final long serialVersionUID = -7432034459257106867L;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "address")
    private String address;

    @Column(name = "email")
    private String email;

    @Column(name = "phone", nullable = false)
    private String phone;

    @Column(name = "status", nullable = false)
    private UserStatus status = UserStatus.ACTIVE;

    @Column(name = "assigned_branch")
    private String assignedBranch;

    @Column(name = "enabled")
    private Boolean enabled = true;

    @Column(name = "deleted", nullable = false)
    private boolean deleted = false;

    @ManyToMany
    @JoinTable(
        name = "users_roles",
        joinColumns = @JoinColumn(
            name = "user_id",
            nullable = false,
            foreignKey = @ForeignKey(
                name = "fk_users_roles_user_id"
            )
        ),
        inverseJoinColumns = @JoinColumn(
            name = "role_id",
            nullable = false,
            foreignKey = @ForeignKey(
                name = "fk_users_roles_role_id"
            )
        )
    )
    @Singular
    private Set<Role> roles;

    @Column(name = "lang_key", length = 10)
    private String langKey;

    @Version
    @Column(name = "version")
    private Long version;

    @Builder
    public User(Long id, String username, String password, String code, String fullName, String address, String email,
                String phone, UserStatus status, String assignedBranch, Boolean enabled, boolean deleted,
                Set<Role> roles, String langKey, Long version) {
        super(id);
        this.username = username;
        this.password = password;
        this.code = code;
        this.fullName = fullName;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.assignedBranch = assignedBranch;
        this.enabled = enabled;
        this.deleted = deleted;
        this.roles = roles;
        this.langKey = langKey;
        this.version = version;
    }
}

