package com.sauravs.reservationapp.model;

import com.sauravs.reservationapp.exception.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum UserStatus {
    ACTIVE(0),
    SUSPENDED(1);

    private int id;

    public static UserStatus getById(int id) {
        return Arrays.stream(UserStatus.values())
            .filter(userStatus -> userStatus.id == id)
            .findAny()
            .orElseThrow(NotFoundException::new);
    }
}
