package com.sauravs.reservationapp.model;

import java.io.Serializable;

public interface Persistable extends Serializable {
    boolean isPersisted();
}