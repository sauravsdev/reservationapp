package com.sauravs.reservationapp.model;

import com.sauravs.reservationapp.exception.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Permission {
    CREATE_USER(0, "CREATE_USER"),
    EDIT_USER(1, "EDIT_USER"),
    VIEW_USER(2, "VIEW_USER"),
    MODIFY_USER(3, "MODIFY_USER"),
    CREATE_REGISTERED_PASSENGER(4, "CREATE_REGISTERED_PASSENGER"),
    EDIT_REGISTERED_PASSENGER(5, "EDIT_REGISTERED_PASSENGER"),
    VIEW_REGISTERED_PASSENGER(6, "VIEW_REGISTERED_PASSENGER"),
    CREATE_ROLE(7, "CREATE_ROLE"),
    EDIT_ROLE(8, "EDIT_ROLE"),
    VIEW_ROLE(9, "VIEW_ROLE");

    private int id;
    private String title;

    public static Permission getById(int id) {
        return Arrays.stream(Permission.values())
            .filter(permission -> permission.id == id)
            .findAny()
            .orElseThrow(NotFoundException::new);
    }
}

