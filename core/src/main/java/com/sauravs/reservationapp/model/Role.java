package com.sauravs.reservationapp.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(
    name = "roles",
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = "name",
            name = "uk_roles_name"
        ),
        @UniqueConstraint(
            columnNames = "code",
            name = "uk_roles_code"
        )
    }
)
public class Role extends BaseEntity implements Versionable {

    private static final long serialVersionUID = 6752188782174453872L;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @ElementCollection(targetClass = Permission.class)
    @CollectionTable(
        name = "roles_permissions",
        joinColumns = @JoinColumn(
            name = "role_id",
            foreignKey = @ForeignKey(
                name = "fk_roles_permissions_role_id"
            )
        )
    )
    @Enumerated(EnumType.STRING)
    @Column(name = "permission")
    private Set<Permission> permissions;

    @Column(name = "deleted")
    private boolean deleted = false;

    @Version
    @Column(name = "version")
    private Long version;

    @Builder
    public Role(Long id, String code, String name, String description,
                Set<Permission> permissions, boolean deleted, Long version) {
        super(id);
        this.code = code;
        this.name = name;
        this.description = description;
        this.permissions = permissions;
        this.deleted = deleted;
        this.version = version;
    }
}

