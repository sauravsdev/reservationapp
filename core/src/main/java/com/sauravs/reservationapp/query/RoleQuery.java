package com.sauravs.reservationapp.query;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleQuery implements Serializable {

    private static final long serialVersionUID = 5534099240437107628L;

    private Long id;
    private String code;
    private String name;
}

