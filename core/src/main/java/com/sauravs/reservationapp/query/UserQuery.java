package com.sauravs.reservationapp.query;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserQuery implements Serializable {

    private static final long serialVersionUID = 7425368321667602084L;

    private Long id;
    private String username;
    private String code;
    private String phone;
    private String email;
}

