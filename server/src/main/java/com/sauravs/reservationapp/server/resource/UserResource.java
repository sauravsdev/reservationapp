package com.sauravs.reservationapp.server.resource;

import com.sauravs.reservationapp.api.data.*;
import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.model.UserStatus;
import com.sauravs.reservationapp.query.UserQuery;
import com.sauravs.reservationapp.server.security.AuthoritiesConstants;
import com.sauravs.reservationapp.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.sauravs.reservationapp.server.util.ResponseUtil.resourceUri;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api/users")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class UserResource {

    @Nonnull
    private final UserService userService;

    @Nonnull
    private final PasswordEncoder passwordEncoder;


    @ApiOperation(value = "saveUser", notes = "Endpoint to save a new User")
    @ApiResponses({@ApiResponse(code = 201, message = "Created", response = Long.class)})
    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> saveUser(
        @ApiParam(name = "saveUserRequest", value = "POST request payload to save new User", required = true)
        @Valid @RequestBody SaveUserRequest request) {

        log.debug("POST request to save User: {}", request);
        return Optional.of(request)
            .map(this::convertSaveUserRequestToUser)
            .map(userService::saveUser)
            .map(User::getId)
            .map(resourceId -> ResponseEntity.created(resourceUri(resourceId)).body(resourceId))
            .orElseThrow(IllegalArgumentException::new);
    }


    @ApiOperation(value = "updateUser", notes = "Endpoint to update a User")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = Long.class)})
    @PutMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> updateUser(
        @ApiParam(name = "id", value = "PUT request PathVariable for User ID")
        @PathVariable Long id,

        @ApiParam(name = "updateUserRequest", value = "PUT request payload to update a User", required = true)
        @Valid @RequestBody UpdateUserRequest request) {

        log.debug("PUT request to update User({}): {}", id, request);

        User existingUser = userService.userById(id);
        return Optional.of(request)
            .map(r -> {
                User user = convertUpdateUserRequestToUser(id, r);
                user.setCode(existingUser.getCode());
                user.setPassword(existingUser.getPassword());
                return user;
            })
            .map(userService::saveUser)
            .map(User::getId)
            .map(resourceId -> ResponseEntity.ok().location(resourceUri(resourceId)).body(resourceId))
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "resetPassword", notes = "Endpoint to reset password of a User")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = Long.class)})
    @PutMapping(value = "/resetPassword/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> resetPassword(
        @ApiParam(name = "id", value = "PUT request PathVariable for User ID")
        @PathVariable Long id,

        @ApiParam(name = "resetPasswordRequest", value = "PUT request payload to reset password", required = true)
        @Valid @RequestBody ResetPasswordRequest request) {

        log.debug("PUT request to reset password of a User");

        return Optional.of(request)
            .map(r -> convertResetPasswordRequestToUser(id, r))
            .map(userService::saveUser)
            .map(User::getId)
            .map(resourceId -> ResponseEntity.ok().body(resourceId))
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "deleteUser", notes = "Endpoint to delete a User")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = Long.class)})
    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> deleteUser(
        @ApiParam(name = "id", value = "DELETE request PathVariable for User ID")
        @PathVariable Long id) {

        log.debug("DELETE request to delete User({})", id);
        return Optional.of(id)
            .map(resourceId -> {
                userService.deleteUser(resourceId);
                return ResponseEntity.ok().body(resourceId);
            })
            .orElseThrow(IllegalStateException::new);
    }

    @ApiOperation(value = "userById", notes = "Endpoint to get a User by ID")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = UserDetails.class)})
    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetails> userById(
        @ApiParam(name = "id", value = "GET request PathVariable for User ID")
        @PathVariable Long id) {

        log.debug("GET request for User({})", id);
        return Optional.of(id)
            .map(userService::userById)
            .map(this::convertUserToUserDetails)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "userByUsername", notes = "Endpoint to get a User by Username")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = UserDetails.class)})
    @GetMapping(value = "/byUsername/{username}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetails> userByUsername(
        @ApiParam(name = "username", value = "GET request PathVariable for User Username")
        @PathVariable String username) {

        log.debug("GET request for User({})", username);
        return Optional.of(username)
            .map(userService::userByUsername)
            .map(this::convertUserToUserDetails)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "userByPage", notes = "Endpoint to get a PageResponse of User")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = UserDetailsPageResponse.class)})
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetailsPageResponse> userByPage(
        @ApiParam(name = "pageNumber", value = "GET request RequestParam for setting pageNumber value")
        @RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,

        @ApiParam(name = "pageSize", value = "GET request RequestParam for setting pageSize value")
        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        log.debug("GET request for User page response ({})({})", pageNumber, pageSize);

        return Optional.of(userService.findAllUser(PageRequest.of(pageNumber, pageSize)))
            .map(page -> ResponseEntity.ok().body(new UserDetailsPageResponse(
                page,
                page.getContent().stream()
                    .map(this::convertUserToUserDetails)
                    .collect(Collectors.toList())
            )))
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "userByPageWithSearch", notes = "Endpoint to get a PageResponse of User with Search")
    @ApiResponses({@ApiResponse(code = 200, message = "OK", response = UserDetailsPageResponse.class)})
    @PostMapping(value = "/search", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetailsPageResponse> userByPageWithSearch(
        @ApiParam(name = "userSearchRequest", value = "POST request payload to search User", required = true)
        @Valid @RequestBody UserSearchRequest request) {

        log.debug("POST request to search User: {}", request);

        PageRequest pageRequest = PageRequest.of(Optional.ofNullable(request.getPageNumber()).orElse(0), Optional.ofNullable(request.getPageSize()).orElse(10));

        UserQuery query = UserQuery.builder()
            .id(request.getId())
            .username(request.getUsername())
            .code(request.getCode())
            .phone(request.getPhone())
            .email(request.getEmail())
            .build();

        return Optional.of(userService.findAllUserWithQuery(pageRequest, query))
            .map(page -> ResponseEntity.ok().body(new UserDetailsPageResponse(
                page,
                page.getContent().stream()
                    .map(this::convertUserToUserDetails)
                    .collect(Collectors.toList())
            )))
            .orElseThrow(IllegalStateException::new);
    }


    private User convertSaveUserRequestToUser(@Nonnull SaveUserRequest request) {
        return User.builder()
            .username(request.getUsername())
            .password(passwordEncoder.encode(request.getPassword()))
            .code("U-" + RandomStringUtils.randomAlphanumeric(5))
            .fullName(request.getFullName())
            .address(request.getAddress())
            .email(request.getEmail())
            .phone(request.getPhone())
            .status(UserStatus.ACTIVE)
            .assignedBranch(request.getAssignedBranch())
            .enabled(true)
            .roles(validateAndGetRoles(request.getRoles()))
            .langKey(request.getLangKey())
            .build();
    }

    private User convertUpdateUserRequestToUser(@Nonnull Long id, @Nonnull UpdateUserRequest request) {
        return User.builder()
            .id(id)
            .username(request.getUsername())
            .fullName(request.getFullName())
            .address(request.getAddress())
            .email(request.getEmail())
            .phone(request.getPhone())
            .status(UserStatus.getById(request.getStatus()))
            .assignedBranch(request.getAssignedBranch())
            .enabled(request.getEnabled())
            .langKey(request.getLangKey())
            .roles(validateAndGetRoles(request.getRoles()))
            .version(request.getVersion())
            .build();
    }

    private User convertResetPasswordRequestToUser(@Nonnull Long id, @Nonnull ResetPasswordRequest request) {
        return Optional.of(id)
            .map(userService::userById)
            .map(user -> {
                user.setPassword(passwordEncoder.encode(request.getPassword()));
                return user;
            })
            .orElseThrow(IllegalStateException::new);
    }

    private Set<Role> validateAndGetRoles(Set<Long> roleSummaries) {
        Set<Role> roles = Collections.singleton(basicUserRole());
        if (!CollectionUtils.isEmpty(roleSummaries)) {
            roles = roleSummaries.stream()
                .map(userService::roleById)
                .collect(Collectors.toSet());
        }
        return roles;
    }

    private Role basicUserRole() {
        return userService.roleByCode(AuthoritiesConstants.BASIC_USER_CODE);
    }

    private UserDetails convertUserToUserDetails(@Nonnull User user) {
        return UserDetails.builder()
            .id(user.getId())
            .username(user.getUsername())
            .code(user.getCode())
            .fullName(user.getFullName())
            .address(user.getAddress())
            .email(user.getEmail())
            .phone(user.getPhone())
            .status(convertUserStatusToUserStatusDetails(user.getStatus()))
            .assignedBranch(user.getAssignedBranch())
            .enabled(user.getEnabled())
            .deleted(user.isDeleted())
            .roles(
                user.getRoles().stream()
                    .map(this::convertRoleToRoleSummary)
                    .collect(Collectors.toSet())
            )
            .langKey(user.getLangKey())
            .version(user.getVersion())
            .build();
    }

    private UserStatusDetails convertUserStatusToUserStatusDetails(@Nonnull UserStatus userStatus) {
        return UserStatusDetails.builder()
            .name(userStatus.name())
            .id(userStatus.getId())
            .build();
    }

    private RoleSummary convertRoleToRoleSummary(@Nonnull Role role) {
        return RoleSummary.builder()
            .id(role.getId())
            .code(role.getCode())
            .name(role.getName())
            .build();
    }
}
