package com.sauravs.reservationapp.server.repository;

import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.query.UserQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import java.util.List;

public interface UserRepositoryCustom {

    @Nonnull
    List<User> findUsersByIdAndStatus(@Nonnull final Long userId, @Nonnull final Integer option);

    @Nonnull
    List<User> findUserByUsernameAndStatus(@Nonnull final String username, @Nonnull final Integer option);

    @Nonnull
    Page<User> findAllUser(@Nonnull final Pageable pageable);

    @Nonnull
    Page<User> findAllUserWithQuery(@Nonnull final Pageable pageable,
                                    @Nonnull final UserQuery userQuery);
}

