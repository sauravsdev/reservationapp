package com.sauravs.reservationapp.server.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Nonnull;

@Configuration
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DataSourceConfiguration {

    @Nonnull
    private final AppProperties appProperties;

    @Bean(destroyMethod = "close")
    public HikariDataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName(appProperties.getDataSourceProperties().getDriverClassName());
        config.setJdbcUrl(appProperties.getDataSourceProperties().getUrl());
        config.setUsername(appProperties.getDataSourceProperties().getUsername());
        config.setPassword(appProperties.getDataSourceProperties().getPassword());
        config.setMaximumPoolSize(appProperties.getDataSourceProperties().getMaximumPoolSize());
        return new HikariDataSource(config);
    }
}
