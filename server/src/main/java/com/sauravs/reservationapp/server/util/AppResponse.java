package com.sauravs.reservationapp.server.util;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.zalando.problem.Problem;

@Builder
@Getter
@Setter
public class AppResponse {
    private Object data;
    private Problem error;
}


