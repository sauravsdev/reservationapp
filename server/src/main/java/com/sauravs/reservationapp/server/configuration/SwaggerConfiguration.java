package com.sauravs.reservationapp.server.configuration;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.singletonList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static springfox.documentation.spi.DocumentationType.SWAGGER_2;
import static springfox.documentation.swagger.web.ApiKeyVehicle.HEADER;


@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket api() {
        return new Docket(SWAGGER_2)
            .apiInfo(apiInfo())
            .securitySchemes(
                singletonList(
                    new ApiKey("JWT", AUTHORIZATION, HEADER.name())
                )
            )
            .securityContexts(
                singletonList(
                    SecurityContext.builder()
                        .securityReferences(
                            singletonList(
                                SecurityReference.builder()
                                    .reference("JWT")
                                    .scopes(ArrayUtils.toArray())
                                    .build()
                            )
                        )
                        .build()
                )
            )
            .useDefaultResponseMessages(false)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build();

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Reservation App Server API")
            .description("Swagger UI for Reservation App Server API")
            .contact(
                new Contact(
                    "Saurav's Dev",
                    "https://sauravsdev.sauravs.com",
                    "contact@sauravs.com"
                )
            )
            .version("1.0.0")
            .build();
    }

}
