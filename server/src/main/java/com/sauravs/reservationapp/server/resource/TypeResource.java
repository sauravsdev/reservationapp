package com.sauravs.reservationapp.server.resource;


import com.sauravs.reservationapp.api.data.PermissionDetails;
import com.sauravs.reservationapp.api.data.UserStatusDetails;
import com.sauravs.reservationapp.model.Permission;
import com.sauravs.reservationapp.model.UserStatus;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/api/types")
@Log4j2
public class TypeResource {

    @ApiOperation(value = "permissionAll", notes = "Endpoint to get a List of all Permission")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", responseContainer = "List", response = PermissionDetails.class)
    })
    @GetMapping(value = "/permission", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PermissionDetails>> permissionAll() {

        log.debug("GET request for a list of all PermissionDetails");

        return ResponseEntity.ok()
            .body(
                Arrays.stream(Permission.values())
                    .map(this::permissionToPermissionDetails)
                    .collect(Collectors.toList())
            );
    }

    @ApiOperation(value = "userStatusAll", notes = "Endpoint to get a List of all UserStatus")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", responseContainer = "List", response = UserStatusDetails.class)
    })
    @GetMapping(value = "/userStatus", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserStatusDetails>> userStatusAll() {

        log.debug("GET request for a list of all UserStatusDetails");

        return ResponseEntity.ok()
            .body(
                Arrays.stream(UserStatus.values())
                    .map(this::userStatusToUserStatusDetails)
                    .collect(Collectors.toList())
            );
    }

    private PermissionDetails permissionToPermissionDetails(@Nonnull Permission permission) {
        return PermissionDetails.builder()
            .name(permission.name())
            .id(permission.getId())
            .title(permission.getTitle())
            .build();
    }

    private UserStatusDetails userStatusToUserStatusDetails(@Nonnull UserStatus userStatus) {
        return UserStatusDetails.builder()
            .name(userStatus.name())
            .id(userStatus.getId())
            .build();
    }
}
