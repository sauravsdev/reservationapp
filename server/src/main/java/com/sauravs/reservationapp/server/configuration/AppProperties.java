package com.sauravs.reservationapp.server.configuration;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

@ConfigurationProperties("reservationapp")
@Getter
public class AppProperties {

    private final Security security = new Security();
    private final CorsConfiguration cors = new CorsConfiguration();
    private final DataSourceProperties dataSourceProperties = new DataSourceProperties();

    @Getter
    public static class Security {

        private final ClientAuthorization clientAuthorization = new ClientAuthorization();
        private final Authentication authentication = new Authentication();
        private final RememberMe rememberMe = new RememberMe();

        @Getter
        @Setter
        public static class ClientAuthorization {
            private String accessTokenUri;
            private String tokenServiceId;
            private String clientId;
            private String clientSecret;
        }

        @Getter
        public static class Authentication {
            private final Jwt jwt = new Jwt();

            @Getter
            @Setter
            public static class Jwt {
                private String secret;
                private String base64Secret;
                private long tokenValidityInSeconds;
                private long tokenValidityInSecondsForRememberMe;
            }
        }

        @Getter
        @Setter
        public static class RememberMe {
            private String key;
        }
    }

    @Getter
    @Setter
    public static class DataSourceProperties {
        private String driverClassName;
        private String protocol;
        private String host;
        private String database;
        private String parameters;
        private String username;
        private String password;
        private Integer maximumPoolSize;

        public String getUrl() {
            return getProtocol() + "//" + getHost() + "/" + getDatabase()
                + (StringUtils.isNotEmpty(getParameters()) ? ("?" + getParameters()) : "");
        }
    }
}
