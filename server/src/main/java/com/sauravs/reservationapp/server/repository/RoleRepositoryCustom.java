package com.sauravs.reservationapp.server.repository;

import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.query.RoleQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;

public interface RoleRepositoryCustom {

    @Nonnull
    Page<Role> findAllRole(@Nonnull final Pageable pageable);

    @Nonnull
    Page<Role> findAllRoleWithQuery(@Nonnull final Pageable pageable, @Nonnull final RoleQuery roleQuery);
}