package com.sauravs.reservationapp.server.resource;

import com.sauravs.reservationapp.api.data.LoginRequest;
import com.sauravs.reservationapp.api.data.TokenDetails;
import com.sauravs.reservationapp.server.security.jwt.JwtFilter;
import com.sauravs.reservationapp.server.security.jwt.TokenProvider;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/api/login")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class LoginResource {

    @Nonnull
    private final TokenProvider tokenProvider;

    @Nonnull
    private final AuthenticationManagerBuilder authenticationManagerBuilder;


    @ApiOperation(value = "login", notes = "Endpoint to login and get a new token")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = TokenDetails.class)
    })
    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenDetails> login(
        @ApiParam(name = "loginRequest", value = "POST request payload to login", required = true)
        @Valid @RequestBody LoginRequest request) {

        log.debug("POST request to login by: {}", request.getUsername());

        return Optional.of(request)
            .map(r -> new UsernamePasswordAuthenticationToken(r.getUsername(), r.getPassword()))
            .map(t -> authenticationManagerBuilder.getObject().authenticate(t))
            .map(a -> {
                SecurityContextHolder.getContext().setAuthentication(a);
                boolean rememberMe = (request.getRememberMe() == null) ? false : request.getRememberMe();
                return tokenProvider.createToken(a, rememberMe);
            })
            .map(t -> ResponseEntity.ok()
                .headers(withBearerHeader(t))
                .body(
                    TokenDetails.builder()
                        .token(t)
                        .build()
                )
            )
            .orElseThrow(IllegalStateException::new);
    }

    private HttpHeaders withBearerHeader(@Nonnull String token) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JwtFilter.AUTHORIZATION_HEADER, JwtFilter.BEARER_PREFIX + token);
        return httpHeaders;
    }
}
