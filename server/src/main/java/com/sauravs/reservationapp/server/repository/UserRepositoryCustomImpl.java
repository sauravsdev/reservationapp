package com.sauravs.reservationapp.server.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.JPQLQuery;
import com.sauravs.reservationapp.model.QUser;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.query.UserQuery;
import com.sauravs.reservationapp.server.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.requireNonNull;

@Component("userCustomRepository")
public class UserRepositoryCustomImpl extends QuerydslRepositorySupport implements UserRepositoryCustom {

    public UserRepositoryCustomImpl() {
        super(User.class);
    }

    @Override
    @Nonnull
    public List<User> findUsersByIdAndStatus(@Nonnull final Long userId, @Nonnull final Integer option) {
        QUser qUser = QUser.user;
        BooleanBuilder predicate = new BooleanBuilder().and(qUser.id.like(userId.toString()));
        buildPredicateBasedOnOption(option, qUser, predicate);
        return from(qUser).where(predicate).fetch();
    }

    @Override
    @Nonnull
    public List<User> findUserByUsernameAndStatus(@Nonnull final String username, @Nonnull final Integer option) {
        QUser qUser = QUser.user;
        BooleanBuilder predicate = new BooleanBuilder().and(qUser.username.like(username));
        buildPredicateBasedOnOption(option, qUser, predicate);
        return from(qUser).where(predicate).fetch();
    }

    @Override
    @Nonnull
    public Page<User> findAllUser(@Nonnull final Pageable pageable) {
        QUser qUser = QUser.user;
        JPQLQuery<User> query = from(qUser)
            .where(qUser.deleted.isFalse())
            .orderBy(qUser.id.asc());
        return PaginationUtil.createPage(checkNotNull(getQuerydsl()), query, pageable);
    }

    @Override
    @Nonnull
    public Page<User> findAllUserWithQuery(@Nonnull final Pageable pageable,
                                           @Nonnull final UserQuery userQuery) {

        QUser user = QUser.user;
        BooleanBuilder predicate = new BooleanBuilder();

        NumberPath<Long> idPath = user.id;
        Optional.ofNullable(userQuery.getId())
            .ifPresent(
                value -> predicate.and(idPath.eq(value))
            );

        StringPath usernamePath = user.username;
        Optional.ofNullable(userQuery.getUsername())
            .ifPresent(
                value -> predicate.and(usernamePath.equalsIgnoreCase(value))
            );

        StringPath codePath = user.code;
        Optional.ofNullable(userQuery.getCode())
            .ifPresent(
                value -> predicate.and(codePath.equalsIgnoreCase(value))
            );

        StringPath phonePath = user.phone;
        Optional.ofNullable(userQuery.getPhone())
            .ifPresent(
                value -> predicate.and(phonePath.equalsIgnoreCase(value))
            );

        StringPath emailPath = user.email;
        Optional.ofNullable(userQuery.getEmail())
            .ifPresent(
                value -> predicate.and(emailPath.startsWithIgnoreCase(value))
            );

        predicate.and(user.deleted.isFalse());

        JPQLQuery<User> query = from(user)
            .where(predicate)
            .orderBy(user.id.asc())
            .select(user);
        return PaginationUtil.createPage(requireNonNull(getQuerydsl()), query, pageable);
    }

    private void buildPredicateBasedOnOption(Integer option, QUser qUser, BooleanBuilder predicate) {
        switch (option) {
            case 1:
                predicate.and(qUser.enabled.eq(true));
                break;
            case 2:
                predicate.and(qUser.enabled.eq(false));
                break;
            default:
                break;
        }
    }
}