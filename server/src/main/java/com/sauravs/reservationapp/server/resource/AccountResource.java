package com.sauravs.reservationapp.server.resource;

import com.sauravs.reservationapp.api.data.*;
import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.model.UserStatus;
import com.sauravs.reservationapp.server.security.exception.InvalidPasswordException;
import com.sauravs.reservationapp.server.util.SecurityUtil;
import com.sauravs.reservationapp.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.sauravs.reservationapp.server.util.ResponseUtil.resourceUri;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/account")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class AccountResource {

    @Nonnull
    private final UserService userService;

    @Nonnull
    private final PasswordEncoder passwordEncoder;

    @Nonnull
    private final ApplicationEventPublisher applicationEventPublisher;


    @ApiOperation(value = "profile", notes = "Endpoint to get user profile")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = UserDetails.class)
    })
    @GetMapping(value = "/profile", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetails> profile() {
        log.debug("GET request for User from Authentication");
        return SecurityUtil.getLoggedInUser()
            .map(userService::userWithRolesByUsername)
            .map(this::userToUserDetails)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "updateProfile", notes = "Endpoint to update user profile")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Long.class)
    })
    @PutMapping(value = "/profile", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> updateProfile(
        @ApiParam(name = "updateProfileRequest", value = "PUT request payload to update user profile", required = true)
        @Valid @RequestBody UpdateProfileRequest request) {

        log.debug("PUT request to update user profile: {}", request);

        User existingUser = SecurityUtil.getLoggedInUser()
            .map(userService::userByUsername)
            .orElseThrow(IllegalStateException::new);

        return Optional.of(request)
            .map(r -> {
                User user = updateProfileRequestToUser(existingUser.getId(), r);
                user.setUsername(existingUser.getUsername());
                user.setPassword(existingUser.getPassword());
                user.setCode(existingUser.getCode());
                user.setStatus(existingUser.getStatus());
                user.setAssignedBranch(existingUser.getAssignedBranch());
                user.setEnabled(existingUser.getEnabled());
                user.setDeleted(existingUser.isDeleted());
                user.setRoles(existingUser.getRoles());
                return user;
            })
            .map(userService::saveUser)
            .map(User::getId)
            .map(resourceId -> ResponseEntity.ok()
                .location(resourceUri(resourceId))
                .body(resourceId)
            )
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "changePassword", notes = "Endpoint to change password")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Long.class)
    })
    @PutMapping(value = "/changePassword", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> changePassword(
        @ApiParam(name = "changePasswordRequest", value = "PUT request payload to change password", required = true)
        @Valid @RequestBody ChangePasswordRequest request) {

        log.debug("PUT request to change password");

        return Optional.of(request)
            .map(this::changePasswordRequestToUser)
            .map(userService::saveUser)
            .map(User::getId)
            .map(resourceId -> ResponseEntity.ok()
                .body(resourceId)
            )
            .orElseThrow(IllegalStateException::new);
    }


    private UserDetails userToUserDetails(@Nonnull User user) {
        return UserDetails.builder()
            .id(user.getId())
            .username(user.getUsername())
            .code(user.getCode())
            .fullName(user.getFullName())
            .address(user.getAddress())
            .email(user.getEmail())
            .phone(user.getPhone())
            .status(userStatusToUserStatusDetails(user.getStatus()))
            .assignedBranch(user.getAssignedBranch())
            .enabled(user.getEnabled())
            .deleted(user.isDeleted())
            .roles(
                user.getRoles().stream()
                    .map(this::roleToRoleSummary)
                    .collect(Collectors.toSet())
            )
            .langKey(user.getLangKey())
            .version(user.getVersion())
            .build();
    }

    private UserStatusDetails userStatusToUserStatusDetails(@Nonnull UserStatus userStatus) {
        return UserStatusDetails.builder()
            .name(userStatus.name())
            .id(userStatus.getId())
            .build();
    }

    private RoleSummary roleToRoleSummary(@Nonnull Role role) {
        return RoleSummary.builder()
            .id(role.getId())
            .code(role.getCode())
            .name(role.getName())
            .build();
    }

    private User updateProfileRequestToUser(@Nonnull Long id, @Nonnull UpdateProfileRequest request) {
        return User.builder()
            .id(id)
            .fullName(request.getFullName())
            .address(request.getAddress())
            .email(request.getEmail())
            .phone(request.getPhone())
            .langKey(request.getLangKey())
            .version(request.getVersion())
            .build();
    }

    private User changePasswordRequestToUser(@Nonnull ChangePasswordRequest request) {
        return SecurityUtil.getLoggedInUser()
            .map(userService::userByUsername)
            .map(user -> {
                if (!passwordEncoder.matches(request.getCurrentPassword(), user.getPassword())) {
                    throw new InvalidPasswordException("Current password does not match.");
                }
                user.setPassword(passwordEncoder.encode(request.getPassword()));
                return user;
            })
            .orElseThrow(IllegalStateException::new);
    }
}
