package com.sauravs.reservationapp.server.exception;

import com.sauravs.reservationapp.exception.*;
import com.sauravs.reservationapp.server.security.exception.InvalidPasswordException;
import com.sauravs.reservationapp.server.util.AppResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.DefaultProblem;
import org.zalando.problem.Problem;
import org.zalando.problem.ProblemBuilder;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait;
import org.zalando.problem.violations.ConstraintViolationProblem;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.sauravs.reservationapp.server.exception.ErrorConstants.*;
import static com.sauravs.reservationapp.server.exception.ErrorType.*;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;
import static org.zalando.problem.Status.INTERNAL_SERVER_ERROR;

/**
 * Controller advice to translate the server side exceptions to client-friendly json
 * structures. The error response follows RFC7807 - Problem Details for HTTP APIs
 * (https://tools.ietf.org/html/rfc7807).
 */
@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling, SecurityAdviceTrait {

    @ExceptionHandler(CodeNotAvailableException.class)
    ResponseEntity<Problem> handleCodeNotAvailableException(final CodeNotAvailableException exception,
                                                            final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, CODE_NOT_AVAILABLE);
        return create(exception, builder.build(), request);
    }

    @ExceptionHandler(NameNotAvailableException.class)
    ResponseEntity<Problem> handleNameNotAvailableException(final NameNotAvailableException exception,
                                                            final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, NAME_NOT_AVAILABLE);
        return create(exception, builder.build(), request);
    }

    @ExceptionHandler(UsernameNotAvailableException.class)
    ResponseEntity<Problem> handleUsernameNotAvailableException(final UsernameNotAvailableException exception,
                                                                final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, USERNAME_NOT_AVAILABLE);
        return create(exception, builder.build(), request);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    ResponseEntity<Problem> handleInvalidPasswordException(final InvalidPasswordException exception,
                                                           final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, INVALID_PASSWORD_EXCEPTION);
        return create(exception, builder.build(), request);
    }

    @ExceptionHandler(RoleNotFoundException.class)
    ResponseEntity<Problem> handleRoleNotFoundException(final RoleNotFoundException exception,
                                                        final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, ROLE_NOT_FOUND);
        return create(exception, builder.build(), request);
    }

    @ExceptionHandler(UserNotFoundException.class)
    ResponseEntity<Problem> handleUserNotFoundException(final UserNotFoundException exception,
                                                        final NativeWebRequest request) {
        ProblemBuilder builder = Problem.builder()
            .withTitle(INTERNAL_SERVER_ERROR.getReasonPhrase())
            .withStatus(INTERNAL_SERVER_ERROR)
            .withDetail(exception.getMessage())
            .with(ERROR_KEY, USER_NOT_FOUND);
        return create(exception, builder.build(), request);
    }

    @SuppressWarnings("All")
    @Override
    public ResponseEntity process(@Nullable ResponseEntity<Problem> entity, NativeWebRequest request) {
        if (entity == null) {
            return null;
        }
        Problem problem = entity.getBody();
        if (!(problem instanceof DefaultProblem || problem instanceof ConstraintViolationProblem)) {
            return new ResponseEntity(
                AppResponse.builder()
                    .error(entity.getBody())
                    .build(),
                entity.getHeaders(),
                entity.getStatusCode()
            );
        }
        ProblemBuilder builder = Problem.builder().withType(fromUri(PROBLEM_BASE_URI)
            .path("/" + resolveType(problem)).build().toUri())
            .withInstance(fromCurrentRequest().build().toUri())
            .withTitle(problem.getTitle())
            .withStatus(problem.getStatus());

        if (problem instanceof ConstraintViolationProblem) {
            builder.with(VIOLATIONS_KEY, ((ConstraintViolationProblem) problem).getViolations())
                .with(ERROR_KEY, VALIDATION_FAILED);
        } else {
            builder.withCause(((DefaultProblem) problem).getCause());
            problem.getParameters().forEach(builder::with);
        }
        return new ResponseEntity(
            AppResponse.builder()
                .error(builder.build())
                .build(),
            entity.getHeaders(),
            entity.getStatusCode());
    }

    private int resolveType(@Nonnull final Problem problem) {
        if (!problem.getParameters().containsKey(ERROR_KEY) && problem.getStatus() != null) {
            return problem.getStatus().getStatusCode();
        } else {
            return ((ErrorType) problem.getParameters().get(ERROR_KEY)).getStatusCode();
        }
    }

}

