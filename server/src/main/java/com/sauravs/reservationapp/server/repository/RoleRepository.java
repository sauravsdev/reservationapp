package com.sauravs.reservationapp.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.sauravs.reservationapp.model.Role;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, RoleRepositoryCustom {

    Optional<Role> findByIdAndDeletedIsFalse(@Nonnull final Long id);

    Optional<Role> findByName(@Nonnull String name);

    Optional<Role> findByCode(@Nonnull String code);

    @Query("SELECT r FROM Role r WHERE r.code IN (:codes)")
    List<Role> findByCodes(@Param("codes") List<String> codes);

    @Modifying
    @Transactional
    @Query(value = "ALTER TABLE roles AUTO_INCREMENT = 1", nativeQuery = true)
    void resetAutoIncrement();

}
