package com.sauravs.reservationapp.server.security.exception;

import org.springframework.security.core.AuthenticationException;

public class UserNotActivatedException extends AuthenticationException {

    private static final long serialVersionUID = -4593649823663389991L;

    public UserNotActivatedException(String message) {
        super(message);
    }
}

