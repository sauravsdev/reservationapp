package com.sauravs.reservationapp.server.security;

public interface AuthoritiesConstants {

    String ANONYMOUS = "ROLE_ANONYMOUS";

    String BASIC_USER_CODE = "BASIC_USER";
    String BASIC_USER_NAME = "Basic User";
    String BASIC_USER_DESCRIPTION = "This role has basic user access.";

    String SUPER_ADMIN_CODE = "SUPER_ADMIN";
    String SUPER_ADMIN_NAME = "Super Administrator";
    String SUPER_ADMIN_DESCRIPTION = "This role has super administrator access.";

    String TEST_USER_USERNAME = "tester";
    String TEST_USER_PASSWORD = "tester";
    String TEST_USER_FULL_NAME = "API Integration Tester";
    String TEST_USER_PHONE = "01711000000";
    String TEST_USER_LANGUAGE = "en";
}