package com.sauravs.reservationapp.server;

import com.sauravs.reservationapp.server.configuration.AppProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@EnableConfigurationProperties({AppProperties.class})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class Application {

    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(Application.class);
        Environment env = app.run(args).getEnvironment();
        log.info(
            "Access URLs:"
                + "\n\n"
                + "----------------------------------------------------------\n"
                + "\t"
                + "Local: \t\thttp://127.0.0.1:{}\n"
                + "\t"
                + "External: \thttp://{}:{}\n"
                + "----------------------------------------------------------\n",
            env.getProperty("server.port"),
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port")
        );
    }
}

