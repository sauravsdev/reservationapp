package com.sauravs.reservationapp.server.repository;

import com.sauravs.reservationapp.model.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

    Optional<User> findByIdAndDeletedIsFalse(@Nonnull final Long userId);

    Optional<User> findByUsername(@Nonnull final String username);

    Optional<User> findByCode(@Nonnull final String code);

    @EntityGraph(attributePaths = "roles")
    Optional<User> findOneWithRolesByUsername(@Nonnull final String username);

    @Modifying
    @Transactional
    @Query(value = "ALTER TABLE users AUTO_INCREMENT = 1", nativeQuery = true)
    void resetAutoIncrement();
}
