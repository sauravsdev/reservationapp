package com.sauravs.reservationapp.server.resource;

import com.google.common.collect.Sets;
import com.sauravs.reservationapp.api.data.*;
import com.sauravs.reservationapp.model.Permission;
import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.query.RoleQuery;
import com.sauravs.reservationapp.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.sauravs.reservationapp.server.util.ResponseUtil.resourceUri;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/api/roles")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class RoleResource {

    @Nonnull
    private final UserService userService;


    @ApiOperation(value = "saveRole", notes = "Endpoint to save a new Role")
    @ApiResponses({
        @ApiResponse(code = 201, message = "Created", response = Long.class)
    })
    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> saveRole(
        @ApiParam(name = "saveRoleRequest", value = "POST request payload to save new Role", required = true)
        @Valid @RequestBody SaveRoleRequest request) {

        log.debug("POST request to save Role: {}", request);
        return Optional.of(request)
            .map(this::saveRoleRequestToRole)
            .map(userService::saveRole)
            .map(Role::getId)
            .map(
                resourceId -> ResponseEntity.created(resourceUri(resourceId))
                    .body(resourceId)
            )
            .orElseThrow(IllegalArgumentException::new);
    }


    @ApiOperation(value = "updateRole", notes = "Endpoint to update a Role")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Long.class)
    })
    @PutMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> updateRole(
        @ApiParam(name = "id", value = "PUT request PathVariable for Role ID")
        @PathVariable Long id,

        @ApiParam(name = "updateRoleRequest", value = "PUT request payload to update a Role", required = true)
        @Valid @RequestBody UpdateRoleRequest request) {

        log.debug("PUT request to update Role({}): {}", id, request);
        return Optional.of(request)
            .filter(input -> {
                userService.roleById(id);
                return true;
            })
            .map(r -> updateRoleRequestToRole(id, r))
            .map(userService::saveRole)
            .map(Role::getId)
            .map(resourceId -> ResponseEntity.ok()
                .location(resourceUri(resourceId))
                .body(resourceId)
            )
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "deleteRole", notes = "Endpoint to delete a Role")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = Long.class)
    })
    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> deleteRole(
        @ApiParam(name = "id", value = "DELETE request PathVariable for Role ID")
        @PathVariable Long id) {

        log.debug("DELETE request to delete Role({})", id);
        return Optional.of(id)
            .map(resourceId -> {
                userService.deleteRole(resourceId);
                return ResponseEntity.ok()
                    .body(resourceId);
            })
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "roleById", notes = "Endpoint to get a Role by ID")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = RoleDetails.class)
    })
    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDetails> roleById(
        @ApiParam(name = "id", value = "GET request PathVariable for Role ID")
        @PathVariable Long id) {

        log.debug("GET request for Role({})", id);
        return Optional.of(id)
            .map(userService::roleById)
            .map(this::roleToRoleDetails)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "roleByCode", notes = "Endpoint to get a Role by Code")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = RoleDetails.class)
    })
    @GetMapping(value = "/byCode/{code}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDetails> roleByCode(
        @ApiParam(name = "code", value = "GET request PathVariable for Role Code")
        @PathVariable String code) {

        log.debug("GET request for Role({})", code);
        return Optional.of(code)
            .map(userService::roleByCode)
            .map(this::roleToRoleDetails)
            .map(ResponseEntity.ok()::body)
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "roleByPage", notes = "Endpoint to get a PageResponse of Role")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = RoleDetailsPageResponse.class)
    })
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDetailsPageResponse> roleByPage(
        @ApiParam(name = "pageNumber", value = "GET request RequestParam for setting pageNumber value")
        @RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber,

        @ApiParam(name = "pageSize", value = "GET request RequestParam for setting pageSize value")
        @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        log.debug("GET request for Role page response ({})({})", pageNumber, pageSize);

        return Optional.of(userService.findAllRole(PageRequest.of(pageNumber, pageSize)))
            .map(page -> ResponseEntity.ok()
                .body(
                    new RoleDetailsPageResponse(
                        page,
                        page.getContent()
                            .stream()
                            .map(this::roleToRoleDetails)
                            .collect(Collectors.toList())
                    )
                )
            )
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "roleByPageWithSearch", notes = "Endpoint to get a PageResponse of Role with Search")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", response = RoleDetailsPageResponse.class)
    })
    @PostMapping(value = "/search", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<RoleDetailsPageResponse> roleByPageWithSearch(
        @ApiParam(name = "roleSearchRequest", value = "POST request payload to search Role", required = true)
        @Valid @RequestBody RoleSearchRequest request) {

        log.debug("POST request to search Role: {}", request);

        PageRequest pageRequest = PageRequest.of(
            Optional.ofNullable(request.getPageNumber()).orElse(0),
            Optional.ofNullable(request.getPageSize()).orElse(10)
        );

        RoleQuery query = RoleQuery.builder()
            .id(request.getId())
            .code(request.getCode())
            .name(request.getName())
            .build();

        return Optional.of(userService.findAllRoleWithQuery(pageRequest, query))
            .map(page -> ResponseEntity.ok()
                .body(
                    new RoleDetailsPageResponse(
                        page,
                        page.getContent()
                            .stream()
                            .map(this::roleToRoleDetails)
                            .collect(Collectors.toList())
                    )
                )
            )
            .orElseThrow(IllegalStateException::new);
    }


    @ApiOperation(value = "roleSummaryAll", notes = "Endpoint to get a List of all RoleSummary")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK", responseContainer = "List", response = RoleSummary.class)
    })
    @GetMapping(value = "/summary/all", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RoleSummary>> roleSummaryAll() {

        log.debug("GET request for a list of all RoleSummary");

        int total = ((Long) userService.findAllRole(PageRequest.of(0, 1)).getTotalElements()).intValue();
        if (total == 0) {
            return ResponseEntity.ok().body(Collections.emptyList());
        }
        return Optional.of(userService.findAllRole(PageRequest.of(0, total)))
            .map(page -> ResponseEntity.ok()
                .body(
                    page.getContent()
                        .stream()
                        .map(this::roleToRoleSummary)
                        .collect(Collectors.toList())
                )
            )
            .orElseThrow(IllegalStateException::new);
    }


    private Role saveRoleRequestToRole(@Nonnull SaveRoleRequest request) {
        return Role.builder()
            .code(request.getCode())
            .name(request.getName())
            .description(request.getDescription())
            .permissions(validatePermissions(request.getPermissions()))
            .build();
    }

    private Role updateRoleRequestToRole(@Nonnull Long id, @Nonnull UpdateRoleRequest request) {
        return Role.builder()
            .id(id)
            .code(request.getCode())
            .name(request.getName())
            .description(request.getDescription())
            .permissions(validatePermissions(request.getPermissions()))
            .version(request.getVersion())
            .build();
    }

    private Set<Permission> validatePermissions(Set<Integer> ids) {
        Set<Permission> permissions = Sets.newHashSet();
        if (!CollectionUtils.isEmpty(ids)) {
            permissions = ids.stream()
                .map(Permission::getById)
                .collect(Collectors.toSet());
        }
        return permissions;
    }

    private RoleDetails roleToRoleDetails(@Nonnull Role role) {
        return RoleDetails.builder()
            .id(role.getId())
            .code(role.getCode())
            .name(role.getName())
            .description(role.getDescription())
            .permissions(
                role.getPermissions()
                    .stream()
                    .map(this::permissionToPermissionDetails)
                    .collect(Collectors.toSet())
            )
            .deleted(role.isDeleted())
            .version(role.getVersion())
            .build();
    }

    private PermissionDetails permissionToPermissionDetails(@Nonnull Permission permission) {
        return PermissionDetails.builder()
            .name(permission.name())
            .id(permission.getId())
            .title(permission.getTitle())
            .build();
    }

    private RoleSummary roleToRoleSummary(Role role) {
        return RoleSummary.builder()
            .id(role.getId())
            .code(role.getCode())
            .name(role.getName())
            .build();
    }

}
