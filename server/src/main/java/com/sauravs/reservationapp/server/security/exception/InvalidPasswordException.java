package com.sauravs.reservationapp.server.security.exception;

public class InvalidPasswordException extends RuntimeException {

    private static final long serialVersionUID = 3896130747793606724L;

    public InvalidPasswordException(String message) {
        super(message);
    }
}

