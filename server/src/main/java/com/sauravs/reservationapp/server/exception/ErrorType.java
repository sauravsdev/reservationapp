package com.sauravs.reservationapp.server.exception;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum ErrorType {

    VALIDATION_FAILED(1001),
    CODE_NOT_AVAILABLE(1002),
    NAME_NOT_AVAILABLE(1003),
    USERNAME_NOT_AVAILABLE(1004),
    INVALID_PASSWORD_EXCEPTION(1005),
    ROLE_NOT_FOUND(1006),
    USER_NOT_FOUND(1007);

    @JsonValue
    private final int statusCode;
}

