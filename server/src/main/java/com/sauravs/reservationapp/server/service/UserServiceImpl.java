package com.sauravs.reservationapp.server.service;

import com.sauravs.reservationapp.exception.*;
import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.query.RoleQuery;
import com.sauravs.reservationapp.query.UserQuery;
import com.sauravs.reservationapp.server.repository.RoleRepository;
import com.sauravs.reservationapp.server.repository.UserRepository;
import com.sauravs.reservationapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.Optional;

import static com.sauravs.reservationapp.server.util.PaginationUtil.epochMilli;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
public class UserServiceImpl implements UserService {

    @Nonnull
    private final UserRepository userRepository;

    @Nonnull
    private final RoleRepository roleRepository;

    @Override
    @Nonnull
    @SuppressWarnings("Duplicates")
    public Role saveRole(@Nonnull final Role role) {
        if (role.getId() == null) {
            Optional.of(roleRepository.findByCode(role.getCode()))
                .filter(ro -> !ro.isPresent())
                .orElseThrow(CodeNotAvailableException::new);
            Optional.of(roleRepository.findByName(role.getName()))
                .filter(ro -> !ro.isPresent())
                .orElseThrow(NameNotAvailableException::new);
        } else {
            Optional.of(roleRepository.findByCode(role.getCode()))
                .filter(ro -> !ro.isPresent() || ro.get().getId().equals(role.getId()))
                .orElseThrow(CodeNotAvailableException::new);
            Optional.of(roleRepository.findByName(role.getName()))
                .filter(ro -> !ro.isPresent() || ro.get().getId().equals(role.getId()))
                .orElseThrow(NameNotAvailableException::new);
        }
        return roleRepository.save(role);
    }

    @Override
    public void deleteRole(@Nonnull final Long id) {
        Role role = roleById(id);
        final long timeSuffix = epochMilli();
        role.setCode(role.getCode() + "_" + timeSuffix);
        role.setName(role.getName() + "_" + timeSuffix);
        role.setDeleted(true);
        roleRepository.save(role);
    }

    @Override
    public Optional<Role> findRoleById(@Nonnull final Long id) {
        return roleRepository.findByIdAndDeletedIsFalse(id);
    }

    @Override
    @Nonnull
    public Role roleById(@Nonnull final Long id) {
        return findRoleById(id).orElseThrow(RoleNotFoundException::new);
    }

    @Override
    public Optional<Role> findRoleByName(@Nonnull final String name) {
        return roleRepository.findByName(name);
    }

    @Override
    @Nonnull
    public Role roleByName(@Nonnull final String name) {
        return findRoleByName(name).orElseThrow(RoleNotFoundException::new);
    }

    @Override
    public Optional<Role> findRoleByCode(@Nonnull final String code) {
        return roleRepository.findByCode(code);
    }

    @Override
    @Nonnull
    public Role roleByCode(@Nonnull final String code) {
        return findRoleByCode(code).orElseThrow(RoleNotFoundException::new);
    }

    @Override
    @Nonnull
    public Page<Role> findAllRole(@Nonnull final Pageable pageable) {
        return roleRepository.findAllRole(pageable);
    }

    @Override
    @Nonnull
    public Page<Role> findAllRoleWithQuery(@Nonnull final Pageable pageable, @Nonnull final RoleQuery query) {
        return roleRepository.findAllRoleWithQuery(pageable, query);
    }

    @Override
    @Nonnull
    public User saveUser(@Nonnull final User user) {
        if (user.getId() == null) {
            Optional.of(userRepository.findByUsername(user.getUsername()))
                .filter(uo -> !uo.isPresent())
                .orElseThrow(UsernameNotAvailableException::new);
            Optional.of(userRepository.findByCode(user.getCode()))
                .filter(uo -> !uo.isPresent())
                .orElseThrow(CodeNotAvailableException::new);
        } else {
            Optional.of(userRepository.findByUsername(user.getUsername()))
                .filter(uo -> !uo.isPresent() || uo.get().getId().equals(user.getId()))
                .orElseThrow(UsernameNotAvailableException::new);
            Optional.of(userRepository.findByCode(user.getCode()))
                .filter(uo -> !uo.isPresent() || uo.get().getId().equals(user.getId()))
                .orElseThrow(CodeNotAvailableException::new);
        }
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(@Nonnull final Long id) {
        User user = userById(id);
        final long timeSuffix = epochMilli();
        user.setUsername(user.getUsername() + "_" + timeSuffix);
        user.setCode(user.getCode() + "_" + timeSuffix);
        user.setDeleted(true);
        userRepository.save(user);
    }

    @Override
    public Optional<User> findUserById(@Nonnull final Long id) {
        return userRepository.findByIdAndDeletedIsFalse(id);
    }

    @Override
    @Nonnull
    public User userById(@Nonnull final Long id) {
        return findUserById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Optional<User> findUserByUsername(@Nonnull final String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    @Nonnull
    public User userByUsername(@Nonnull final String username) {
        return findUserByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Optional<User> findUserByCode(@Nonnull final String code) {
        return userRepository.findByCode(code);
    }

    @Override
    @Nonnull
    public User userByCode(@Nonnull final String code) {
        return findUserByCode(code).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Optional<User> findUserWithRolesByUsername(@Nonnull final String username) {
        return userRepository.findOneWithRolesByUsername(username);
    }

    @Override
    @Nonnull
    public User userWithRolesByUsername(@Nonnull final String username) {
        return findUserWithRolesByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Nonnull
    public Page<User> findAllUser(@Nonnull final Pageable pageable) {
        return userRepository.findAllUser(pageable);
    }

    @Override
    @Nonnull
    public Page<User> findAllUserWithQuery(@Nonnull final Pageable pageable, @Nonnull final UserQuery query) {
        return userRepository.findAllUserWithQuery(pageable, query);
    }
}