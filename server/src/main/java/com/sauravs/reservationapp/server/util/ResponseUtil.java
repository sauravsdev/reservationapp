package com.sauravs.reservationapp.server.util;

import java.net.URI;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest;

public interface ResponseUtil {

    static <T> URI resourceUri(T resourceId) {
        requireNonNull(resourceId);
        return Optional.of(resourceId)
            .map(id -> fromCurrentRequest().path("/{id}").buildAndExpand(id).toUri())
            .orElseThrow(IllegalStateException::new);
    }
}

