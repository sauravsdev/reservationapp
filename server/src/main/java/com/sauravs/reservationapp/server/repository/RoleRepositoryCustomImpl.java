package com.sauravs.reservationapp.server.repository;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Component;
import com.sauravs.reservationapp.model.QRole;
import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.query.RoleQuery;
import com.sauravs.reservationapp.server.util.PaginationUtil;

import javax.annotation.Nonnull;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

@Component("roleRepositoryCustom")
public class RoleRepositoryCustomImpl extends QuerydslRepositorySupport implements RoleRepositoryCustom {

    public RoleRepositoryCustomImpl() {
        super(Role.class);
    }

    @Override
    @Nonnull
    public Page<Role> findAllRole(@Nonnull final Pageable pageable) {
        QRole role = QRole.role;
        JPQLQuery<Role> query = from(role)
            .where(role.deleted.isFalse())
            .orderBy(role.id.asc())
            .select(role);
        return PaginationUtil.createPage(requireNonNull(getQuerydsl()), query, pageable);
    }

    @Override
    @Nonnull
    public Page<Role> findAllRoleWithQuery(@Nonnull final Pageable pageable,
                                           @Nonnull final RoleQuery roleQuery) {

        QRole role = QRole.role;
        BooleanBuilder predicate = new BooleanBuilder();

        NumberPath<Long> idPath = role.id;
        Optional.ofNullable(roleQuery.getId())
            .ifPresent(
                value -> predicate.and(idPath.eq(value))
            );

        StringPath codePath = role.code;
        Optional.ofNullable(roleQuery.getCode())
            .ifPresent(
                value -> predicate.and(codePath.equalsIgnoreCase(value))
            );

        StringPath namePath = role.name;
        Optional.ofNullable(roleQuery.getName())
            .ifPresent(
                value -> predicate.and(namePath.startsWithIgnoreCase(value))
            );

        predicate.and(role.deleted.isFalse());

        JPQLQuery<Role> query = from(role)
            .where(predicate)
            .orderBy(role.id.asc())
            .select(role);
        return PaginationUtil.createPage(requireNonNull(getQuerydsl()), query, pageable);
    }

}