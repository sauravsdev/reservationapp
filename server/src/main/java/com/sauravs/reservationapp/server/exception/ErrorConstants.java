package com.sauravs.reservationapp.server.exception;

import java.net.URI;

public interface ErrorConstants {
    String ERROR_KEY = "error";
    String VIOLATIONS_KEY = "violations";
    String PROBLEM_BASE_URL = "https://freeway-admin/problem";
    URI PROBLEM_BASE_URI = URI.create(PROBLEM_BASE_URL);
}

