package com.sauravs.reservationapp.server.security;

import com.sauravs.reservationapp.model.Role;
import com.sauravs.reservationapp.model.User;
import com.sauravs.reservationapp.server.repository.UserRepository;
import com.sauravs.reservationapp.server.security.exception.UserNotActivatedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Component("userDetailsService")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Log4j2
public class AppUserDetailsService implements UserDetailsService {

    @Nonnull
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findOneWithRolesByUsername(username)
            .map(user -> createSpringSecurityUser(username, user))
            .orElseThrow(
                () -> new UsernameNotFoundException("User with  " + username + " was not found in the database.")
            );
    }

    private org.springframework.security.core.userdetails.User createSpringSecurityUser(String username,
                                                                                        User user) {
        if (!user.getEnabled()) {
            throw new UserNotActivatedException("User <" + username + "> is not activated.");
        }
        List<GrantedAuthority> grantedAuthorities = user.getRoles()
            .stream()
            .map(Role::getCode)
            .map(code -> ("ROLE_" + code))
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
        return new org.springframework.security.core.userdetails.User(
            user.getUsername(),
            user.getPassword(),
            grantedAuthorities
        );
    }

}

