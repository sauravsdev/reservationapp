package com.sauravs.reservationapp.validation.validator;

import com.sauravs.reservationapp.validation.annotation.Username;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<Username, String> {

    private Boolean allowNull;
    private static final String USERNAME_PATTERN = "^(?!.*[-_.]{2,})(?=^[^-_.].*[^-_.]$)[\\w-_.]{3,50}$";

    @Override
    public void initialize(Username constraintAnnotation) {
        allowNull = constraintAnnotation.allowNull();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return allowNull;
        }
        return value.matches(USERNAME_PATTERN);
    }
}
