package com.sauravs.reservationapp.validation.validator;

import com.sauravs.reservationapp.validation.annotation.Language;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LanguageValidator implements ConstraintValidator<Language, String> {

    private Boolean allowNull;
    private static final List<String> SUPPORTED_LANGUAGE_LIST = Collections.unmodifiableList(
            Arrays.asList(
                    "en",
                    "bn"
            )
    );

    @Override
    public void initialize(Language constraintAnnotation) {
        allowNull = constraintAnnotation.allowNull();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return allowNull;
        }
        return SUPPORTED_LANGUAGE_LIST.contains(value);
    }
}
