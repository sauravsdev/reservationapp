package com.sauravs.reservationapp.validation.validator;

import com.sauravs.reservationapp.validation.annotation.Email;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmailValidator implements ConstraintValidator<Email, String> {

    private Boolean allowNull;
    private static final String EMAIL_PATTERN = "^[\\w-_.%+]+@[\\w-.]+\\.[A-Za-z]{2,64}$";

    @Override
    public void initialize(Email constraintAnnotation) {
        allowNull = constraintAnnotation.allowNull();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return allowNull;
        }
        return value.matches(EMAIL_PATTERN);
    }
}

