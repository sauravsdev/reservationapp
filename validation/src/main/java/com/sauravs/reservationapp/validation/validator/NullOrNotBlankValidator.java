package com.sauravs.reservationapp.validation.validator;

import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NullOrNotBlankValidator implements ConstraintValidator<NullOrNotBlank, String> {

    @Override
    public void initialize(NullOrNotBlank parameters) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null) {
            return true;
        }
        if (value.length() == 0) {
            return false;
        }
        boolean isAllWhitespace = value.matches("^\\s*$");
        return !isAllWhitespace;
    }

}
