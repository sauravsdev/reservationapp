package com.sauravs.reservationapp.validation.validator;

public interface ConfirmablePassword {

    String getPassword();

    String getConfirmPassword();
}
