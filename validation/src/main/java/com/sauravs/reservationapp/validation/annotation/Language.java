package com.sauravs.reservationapp.validation.annotation;

import com.sauravs.reservationapp.validation.validator.LanguageValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = LanguageValidator.class)
public @interface Language {

    String message() default "Invalid Language";

    Class<?>[] groups() default {};

    boolean allowNull() default false;

    Class<? extends Payload>[] payload() default {};
}
