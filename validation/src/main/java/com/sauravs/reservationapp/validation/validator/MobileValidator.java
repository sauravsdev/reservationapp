package com.sauravs.reservationapp.validation.validator;

import com.sauravs.reservationapp.validation.annotation.Mobile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MobileValidator implements ConstraintValidator<Mobile, String> {

    private Boolean allowNull;
    private static final String MOBILE_PATTERN = "^(013|014|015|016|017|018|019)\\d{8}$";

    @Override
    public void initialize(Mobile constraintAnnotation) {
        allowNull = constraintAnnotation.allowNull();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return allowNull;
        }
        return value.matches(MOBILE_PATTERN);
    }
}
