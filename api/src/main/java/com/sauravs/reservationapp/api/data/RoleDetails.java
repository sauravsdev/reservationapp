package com.sauravs.reservationapp.api.data;

import lombok.*;

import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
public class RoleDetails {
    private Long id;
    private String code;
    private String name;
    private String description;
    private Integer reportViewDays;
    private Set<PermissionDetails> permissions;
    private boolean deleted;
    private Long version;
}
