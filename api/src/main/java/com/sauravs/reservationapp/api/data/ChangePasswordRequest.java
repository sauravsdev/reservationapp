package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.MatchPassword;
import com.sauravs.reservationapp.validation.annotation.Password;
import com.sauravs.reservationapp.validation.validator.ConfirmablePassword;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "changePasswordRequest")
@MatchPassword(message = "MatchPassword.changePasswordRequest.confirmPassword")
public class ChangePasswordRequest implements ConfirmablePassword {

    @ApiModelProperty(
        value = "String to set currentPassword property of new ChangePasswordRequest",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.changePasswordRequest.currentPassword")
    private String currentPassword;

    @ApiModelProperty(
        value = "String to set password property of new ChangePasswordRequest",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.changePasswordRequest.password")
    private String password;

    @ApiModelProperty(
        value = "String to set confirmPassword property of new ChangePasswordRequest",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.changePasswordRequest.confirmPassword")
    private String confirmPassword;

}
