package com.sauravs.reservationapp.api.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@ApiModel(value = "pageRequest")
public class PageRequest {

    @ApiModelProperty(
        value = "Integer to set the page number of PageResponse",
        allowableValues = "null or any Integer greater than -1"
    )
    @Min(value = 0, message = "Min.pageRequest.pageNumber")
    private Integer pageNumber;

    @ApiModelProperty(
        value = "Integer to set the page size of PageResponse",
        allowableValues = "null or any Integer greater than 0"
    )
    @Min(value = 1, message = "Min.pageRequest.pageSize")
    private Integer pageSize;
}
