package com.sauravs.reservationapp.api.data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class PageResponse<T> {

    private Page page;
    private List<T> content;

    final public int getTotalPages() {
        return page.getTotalPages();
    }

    final public long getTotalElements() {
        return page.getTotalElements();
    }

    final public int getNumberOfElements() {
        return page.getNumberOfElements();
    }

    final public int getPageNumber() {
        return page.getNumber();
    }

    final public int getPageSize() {
        return page.getSize();
    }

    final public boolean isFirst() {
        return page.isFirst();
    }

    final public boolean isLast() {
        return page.isLast();
    }

    final public List<T> getContent() {
        return content;
    }

}
