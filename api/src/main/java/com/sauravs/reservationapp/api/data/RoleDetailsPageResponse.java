package com.sauravs.reservationapp.api.data;

import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;
import java.util.List;

public class RoleDetailsPageResponse extends PageResponse<RoleDetails> {

    public RoleDetailsPageResponse(@Nonnull Page page, @Nonnull List<RoleDetails> content) {
        super(page, content);
    }
}
