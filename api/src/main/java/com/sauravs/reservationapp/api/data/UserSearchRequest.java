package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@ApiModel(value = "userSearchRequest")
public class UserSearchRequest extends PageRequest {

    @ApiModelProperty(
        value = "Long to match with id property of User that equals with the value",
        allowableValues = "null or any Integer"
    )
    private Long id;

    @ApiModelProperty(
        value = "String to match with username property of User that equals with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String username;

    @ApiModelProperty(
        value = "String to match with code property of User that equals with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String code;

    @ApiModelProperty(
        value = "String to match with phone property of User that equals with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String phone;

    @ApiModelProperty(
        value = "String to match with email property of User that starts with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String email;

    @Builder
    public UserSearchRequest(@Min(value = 0, message = "Min.pageRequest.pageNumber") Integer pageNumber,
                             @Min(value = 1, message = "Min.pageRequest.pageSize") Integer pageSize, Long id,
                             String username, String code, String phone, String email) {
        super(pageNumber, pageSize);
        this.id = id;
        this.username = username;
        this.code = code;
        this.phone = phone;
        this.email = email;
    }
}
