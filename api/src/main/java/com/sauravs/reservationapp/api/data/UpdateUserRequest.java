package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "updateUserRequest")
public class UpdateUserRequest {

    @ApiModelProperty(
        value = "String to update username property of the User",
        required = true,
        allowableValues = "not blank String with length max 50"
    )
    @Username(message = "Username.updateUserRequest.username")
    private String username;

    @ApiModelProperty(
        value = "String to update fullName property of the User",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.updateUserRequest.fullName")
    @Size(max = 255, message = "Size.updateUserRequest.fullName")
    private String fullName;

    @ApiModelProperty(
        value = "String to update address property of the User",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.updateUserRequest.address")
    @Size(max = 255, message = "Size.updateUserRequest.address")
    private String address;

    @ApiModelProperty(
        value = "String to update email property of the User",
        allowableValues = "null or not blank String with length max 100"
    )
    @Email(allowNull = true, message = "Email.updateUserRequest.email")
    @Size(max = 100, message = "Size.updateUserRequest.email")
    private String email;

    @ApiModelProperty(
        value = "String to update phone property of the User",
        required = true,
        allowableValues = "not blank String with length max 30"
    )
    @Mobile(message = "Mobile.updateUserRequest.phone")
    @Size(max = 30, message = "Size.updateUserRequest.phone")
    private String phone;

    @ApiModelProperty(
        value = "Integer to find a UserStatus by id property and to update status property of the User",
        required = true,
        allowableValues = "not null Integer"
    )
    @NotNull(message = "NotNull.updateUserRequest.status")
    private Integer status;

    @ApiModelProperty(
        value = "String to find a Branch by code property and to update assignedBranch property of the User",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.updateUserRequest.assignedBranch")
    @Size(max = 255, message = "Size.updateUserRequest.assignedBranch")
    private String assignedBranch;

    @ApiModelProperty(
        value = "Boolean to update enabled property of the User",
        required = true,
        allowableValues = "true, false"
    )
    @NotNull(message = "NotNull.updateUserRequest.enabled")
    private Boolean enabled;

    @ApiModelProperty(
        value = "String to update langKey property of the User",
        required = true,
        allowableValues = "not blank String"
    )
    @Language(message = "Language.updateUserRequest.langKey")
    private String langKey;

    @ApiModelProperty(
        value = "Set of Long to find roles by id property and to update roles property of the User",
        dataType = "[Ljava.lang.Long;",
        allowableValues = "array of Long"
    )
    private Set<Long> roles;

    @ApiModelProperty(
        value = "Long to set the version property of the previously retrieved instance of User",
        required = true,
        allowableValues = "not null Long"
    )
    @NotNull(message = "NotNull.updateUserRequest.version")
    private Long version;
}
