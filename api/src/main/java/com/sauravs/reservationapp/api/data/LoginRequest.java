package com.sauravs.reservationapp.api.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@ApiModel(value = "loginRequest")
public class LoginRequest {

    @ApiModelProperty(
        value = "String to set username property of new Login request",
        required = true,
        allowableValues = "not blank String with length max 50"
    )
    @NotBlank(message = "NotBlank.loginRequest.username")
    @Size(max = 50, message = "Size.loginRequest.username")
    private String username;

    @ApiModelProperty(
        value = "String to set password property of new Login request",
        required = true,
        allowableValues = "not blank String with length max 50"
    )
    @NotBlank(message = "NotBlank.loginRequest.password")
    @Size(max = 50, message = "Size.loginRequest.password")
    private String password;

    @ApiModelProperty(
        value = "Boolean to set rememberMe property of new Login request"
    )
    private Boolean rememberMe;

}
