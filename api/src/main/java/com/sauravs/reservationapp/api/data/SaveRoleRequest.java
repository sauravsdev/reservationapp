package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "saveRoleRequest")
public class SaveRoleRequest {

    @ApiModelProperty(
        value = "String to set code property of new Role",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.saveRoleRequest.code")
    @Size(max = 255, message = "Size.saveRoleRequest.code")
    private String code;

    @ApiModelProperty(
        value = "String to set name property of new Role",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.saveRoleRequest.name")
    @Size(max = 255, message = "Size.saveRoleRequest.name")
    private String name;

    @ApiModelProperty(
        value = "String to set description property of new Role",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.saveRoleRequest.description")
    @Size(max = 255, message = "Size.saveRoleRequest.description")
    private String description;

    @ApiModelProperty(
        value = "Set of Integer to find permissions by id property and to set permissions property of new Role",
        dataType = "[Ljava.lang.Integer;",
        allowableValues = "array of Integer"
    )
    private Set<Integer> permissions;
}
