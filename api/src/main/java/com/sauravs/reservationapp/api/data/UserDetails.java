package com.sauravs.reservationapp.api.data;

import lombok.*;

import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class UserDetails {

    private Long id;
    private String username;
    private String code;
    private String fullName;
    private String address;
    private String email;
    private String phone;
    private UserStatusDetails status;
    private String assignedBranch;
    private Boolean enabled;
    private boolean deleted;
    private Set<RoleSummary> roles;
    private String langKey;
    private Long version;
}
