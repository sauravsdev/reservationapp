package com.sauravs.reservationapp.api.data;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
public class RoleSummary {
    private Long id;
    private String code;
    private String name;
}
