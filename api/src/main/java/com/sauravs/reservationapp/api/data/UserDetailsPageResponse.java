package com.sauravs.reservationapp.api.data;

import org.springframework.data.domain.Page;

import javax.annotation.Nonnull;
import java.util.List;

public class UserDetailsPageResponse extends PageResponse<UserDetails> {

    public UserDetailsPageResponse(@Nonnull Page page, @Nonnull List<UserDetails> content) {
        super(page, content);
    }
}
