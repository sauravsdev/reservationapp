package com.sauravs.reservationapp.api.data;

import lombok.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserStatusDetails {
    private String name;
    private Integer id;
}
