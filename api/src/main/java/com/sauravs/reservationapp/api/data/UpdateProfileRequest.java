package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.Email;
import com.sauravs.reservationapp.validation.annotation.Language;
import com.sauravs.reservationapp.validation.annotation.Mobile;
import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "updateProfileRequest")
public class UpdateProfileRequest {

    @ApiModelProperty(
        value = "String to update fullName property of the User",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.updateProfileRequest.fullName")
    @Size(max = 255, message = "Size.updateProfileRequest.fullName")
    private String fullName;

    @ApiModelProperty(
        value = "String to update address property of the User",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.updateProfileRequest.address")
    @Size(max = 255, message = "Size.updateProfileRequest.address")
    private String address;

    @ApiModelProperty(
        value = "String to update email property of the User",
        allowableValues = "null or not blank String with length max 100"
    )
    @Email(allowNull = true, message = "Email.updateProfileRequest.email")
    @Size(max = 100, message = "Size.updateProfileRequest.email")
    private String email;

    @ApiModelProperty(
        value = "String to update phone property of the User",
        required = true,
        allowableValues = "not blank String with length max 30"
    )
    @Mobile(message = "Mobile.updateProfileRequest.phone")
    @Size(max = 30, message = "Size.updateProfileRequest.phone")
    private String phone;

    @ApiModelProperty(
        value = "String to update langKey property of the User",
        required = true,
        allowableValues = "not blank String"
    )
    @Language(message = "Language.updateProfileRequest.langKey")
    private String langKey;

    @ApiModelProperty(
        value = "Long to set the version property of the previously retrieved instance of User",
        required = true,
        allowableValues = "not null Long"
    )
    @NotNull(message = "NotNull.updateProfileRequest.version")
    private Long version;
}
