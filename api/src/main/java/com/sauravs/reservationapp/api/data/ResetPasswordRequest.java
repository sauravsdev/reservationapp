package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.MatchPassword;
import com.sauravs.reservationapp.validation.annotation.Password;
import com.sauravs.reservationapp.validation.validator.ConfirmablePassword;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "resetPasswordRequest")
@MatchPassword(message = "MatchPassword.resetPasswordRequest.confirmPassword")
public class ResetPasswordRequest implements ConfirmablePassword {

    @ApiModelProperty(
        value = "String to set password property of new ResetPasswordRequest",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.resetPasswordRequest.password")
    private String password;

    @ApiModelProperty(
        value = "String to set confirmPassword property of new ResetPasswordRequest",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.resetPasswordRequest.confirmPassword")
    private String confirmPassword;
}
