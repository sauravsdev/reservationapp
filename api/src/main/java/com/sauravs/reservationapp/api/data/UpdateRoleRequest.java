package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "updateRoleRequest")
public class UpdateRoleRequest {

    @ApiModelProperty(
        value = "String to update code property of the Role",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.updateRoleRequest.code")
    @Size(max = 255, message = "Size.updateRoleRequest.code")
    private String code;

    @ApiModelProperty(
        value = "String to update name property of the Role",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.updateRoleRequest.name")
    @Size(max = 255, message = "Size.updateRoleRequest.name")
    private String name;

    @ApiModelProperty(
        value = "String to update description property of the Role",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.updateRoleRequest.description")
    @Size(max = 255, message = "Size.updateRoleRequest.description")
    private String description;

    @ApiModelProperty(
        value = "Integer to update reportViewDays property of the Role",
        allowableValues = "range[-1, infinity]"
    )
    @Min(value = -1, message = "Min.updateRoleRequest.reportViewDays")
    private Integer reportViewDays;

    @ApiModelProperty(
        value = "Set of Integer to find permissions by id property and to update permissions property of the Role",
        dataType = "[Ljava.lang.Integer;",
        allowableValues = "array of Integer"
    )
    private Set<Integer> permissions;

    @ApiModelProperty(
        value = "Long to set the version property of the previously retrieved instance of Role",
        required = true,
        allowableValues = "not null Long"
    )
    @NotNull(message = "NotNull.updateRoleRequest.version")
    private Long version;
}
