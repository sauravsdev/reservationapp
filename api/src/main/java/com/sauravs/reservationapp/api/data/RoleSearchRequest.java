package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.NullOrNotBlank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Min;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@ApiModel(value = "roleSearchRequest")
public class RoleSearchRequest extends PageRequest {

    @ApiModelProperty(
        value = "Long to match with id property of Role that equals with the value",
        allowableValues = "null or any Integer"
    )
    private Long id;

    @ApiModelProperty(
        value = "String to match with code property of Role that equals with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String code;

    @ApiModelProperty(
        value = "String to match with name property of Role that starts with the value",
        allowableValues = "null or not blank String"
    )
    @NullOrNotBlank
    private String name;

    @Builder
    public RoleSearchRequest(@Min(value = 0, message = "Min.pageRequest.pageNumber") Integer pageNumber,
                             @Min(value = 1, message = "Min.pageRequest.pageSize") Integer pageSize, Long id,
                             String code, String name) {
        super(pageNumber, pageSize);
        this.id = id;
        this.code = code;
        this.name = name;
    }
}
