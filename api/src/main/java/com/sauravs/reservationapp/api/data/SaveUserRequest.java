package com.sauravs.reservationapp.api.data;

import com.sauravs.reservationapp.validation.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "saveUserRequest")
public class SaveUserRequest {

    @ApiModelProperty(
        value = "String to set username property of new User",
        required = true,
        allowableValues = "not blank String"
    )
    @Username(message = "Username.saveUserRequest.username")
    private String username;

    @ApiModelProperty(
        value = "String to set password property of new User",
        required = true,
        allowableValues = "not blank String"
    )
    @Password(message = "Password.saveUserRequest.password")
    private String password;

    @ApiModelProperty(
        value = "String to set fullName property of new User",
        required = true,
        allowableValues = "not blank String with length max 255"
    )
    @NotBlank(message = "NotBlank.saveUserRequest.fullName")
    @Size(max = 255, message = "Size.saveUserRequest.fullName")
    private String fullName;

    @ApiModelProperty(
        value = "String to set address property of new User",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.saveUserRequest.address")
    @Size(max = 255, message = "Size.saveUserRequest.address")
    private String address;

    @ApiModelProperty(
        value = "String to set email property of new User",
        allowableValues = "null or not blank String with length max 100"
    )
    @Email(allowNull = true, message = "Email.saveUserRequest.email")
    @Size(max = 100, message = "Size.saveUserRequest.email")
    private String email;

    @ApiModelProperty(
        value = "String to set phone property of new User",
        required = true,
        allowableValues = "not blank String with length max 30"
    )
    @Mobile(message = "Mobile.saveUserRequest.phone")
    @Size(max = 30, message = "Size.saveUserRequest.phone")
    private String phone;

    @ApiModelProperty(
        value = "String to find a Branch by code property and to set assignedBranch property of new User",
        allowableValues = "null or not blank String with length max 255"
    )
    @NullOrNotBlank(message = "NullOrNotBlank.saveUserRequest.assignedBranch")
    @Size(max = 255, message = "Size.saveUserRequest.assignedBranch")
    private String assignedBranch;

    @ApiModelProperty(
        value = "String to set langKey property of new User",
        required = true,
        allowableValues = "not blank String"
    )
    @Language(message = "Language.saveUserRequest.langKey")
    private String langKey;

    @ApiModelProperty(
        value = "Set of Long to find roles by id property and to set roles property of new User",
        dataType = "[Ljava.lang.Long;",
        allowableValues = "array of Long"
    )
    private Set<Long> roles;
}
